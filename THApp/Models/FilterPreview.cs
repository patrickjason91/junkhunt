﻿using Nokia.Graphics.Imaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace THApp.Models
{
    public class FilterPreview
    {
        public string FilterName
        {
            get;
            set;
        }

        public IFilter Filter
        {
            get;
            set;
        }

        //public BitmapSource ProcImage
        //{
        //    get
        //    {
        //        return GetAppliedFilter(PhotoStream);
        //    }
        //}

        public List<IFilter> Filters
        {
            get
            {
                return new List<IFilter> { Filter };
            }
        }

        public Stream PhotoStream
        {
            get;
            set;
        }

        public async Task<BitmapSource> GetAppliedFilter(Stream stream)
        {
            BitmapImage bi = new BitmapImage();
            bi.SetSource(stream);
            var height = bi.PixelHeight;
            var width = bi.PixelWidth;
            var src = new StreamImageSource(stream);
            IFilter[] filter = { Filter };
            FilterEffect eff = new FilterEffect();
            eff.Filters = filter;
            WriteableBitmap wb = new WriteableBitmap(bi);
            WriteableBitmapRenderer r = new WriteableBitmapRenderer(src, wb);
            wb = await r.RenderAsync();
            return wb;
        }
    }
}
