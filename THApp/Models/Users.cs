﻿    using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THApp.Models
{
    public class Users : INotifyPropertyChanged
    {
        public int Id { get; set; }

        private string _FirstName;
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName 
        {
             get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
                RaisePropertyChanged("FirstName");
            }
        }


        private string _LastName;
        [JsonProperty(PropertyName = "lastName")]
        public string LastName 
        {
             get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
                RaisePropertyChanged("LastName");
            }
        }

        private string _EmailAddress;
        [JsonProperty(PropertyName = "emailAddress")]
        public string EmailAddress
        {
            get 
            {
                return _EmailAddress;
            }
            set
            {
                _EmailAddress = value;
                RaisePropertyChanged("EmailAddress");
            }
        }

        private string _Password;
        [JsonProperty(PropertyName = "password")]
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
                RaisePropertyChanged("Password");
            }
        }

        private string _imageUri;
        [JsonProperty(PropertyName = "imageUri")]
        public string imageUri
        {
            get
            {
                return _imageUri;
            }
            set
            {
                _imageUri = value;
                RaisePropertyChanged("imageUri");
            }
        }

        private string _containerName;
        [JsonProperty(PropertyName = "containerName")]
        public string containerName
        {
            get
            {
                return _containerName;
            }
            set
            {
                _containerName = value;
                RaisePropertyChanged("containerName");
            }
        }

        private string _resourceName;
        [JsonProperty(PropertyName = "resourceName")]
        public string resourceName
        {
            get
            {
                return _resourceName;
            }
            set
            {
                _resourceName = value;
                RaisePropertyChanged("resourceName");
            }
        }

        private string _sasQueryString;
        [JsonProperty(PropertyName = "sasQueryString")]
        public string sasQueryString
        {
            get
            {
                return _sasQueryString;
            }
            set
            {
                _sasQueryString = value;
                RaisePropertyChanged("sasQueryString");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
