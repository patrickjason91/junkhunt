﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THApp.Models
{
    public class Junks : INotifyPropertyChanged
    {
        public string Id { get; set; }

        private int _userID;
        [JsonProperty(PropertyName = "userID")]
        public int userID
        {
            get
            {
                return _userID;
            }
            set
            {
                _userID = value;
                RaisePropertyChanged("userID");
            }
        }

        private double _latitude;
        [JsonProperty(PropertyName = "latitude")]
        public double latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
                RaisePropertyChanged("latitude");
            }
        }

        private string _name;
        [JsonProperty(PropertyName = "Name")]
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                RaisePropertyChanged("name");
            }
        }

        private DateTime _dateUploaded;
        [JsonProperty(PropertyName = "dateUpload")]
        public DateTime dateUploaded
        {
            get
            {
                return _dateUploaded;
            }
            set
            {
                _dateUploaded = value;
                RaisePropertyChanged("dateUpload");
            }
        }


        private double _longtitude;
        [JsonProperty(PropertyName = "longtitude")]
        public double longtitude
        {
            get
            {
                return _longtitude;
            }
            set
            {
                _longtitude = value;
                RaisePropertyChanged("longtitude");
            }
        }

        private string _location;
        [JsonProperty(PropertyName = "location")]
        public string location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
                RaisePropertyChanged("location");
            }
        }

        private bool _othersApproved;
        [JsonProperty(PropertyName = "othersApproved")]
        public bool othersApproved
        {
            get
            {
                return _othersApproved;
            }
            set
            {
                _othersApproved = value;
                RaisePropertyChanged("othersApproved");
            }
        }

        private bool _ownersApproved;
        [JsonProperty(PropertyName = "ownersApproved")]
        public bool ownersApproved
        {
            get
            {
                return _ownersApproved;
            }
            set
            {
                _ownersApproved = value;
                RaisePropertyChanged("ownersApproved");
            }
        }

        private string _description;
        [JsonProperty(PropertyName = "description")]
        public string description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                RaisePropertyChanged("description");
            }
        }

        private string _imageUri;
        [JsonProperty(PropertyName = "imageUri")]
        public string imageUri
        {
            get
            {
                return _imageUri;
            }
            set
            {
                _imageUri = value;
                RaisePropertyChanged("imageUri");
            }
        }

        private string _containerName;
        [JsonProperty(PropertyName = "containerName")]
        public string containerName
        {
            get
            {
                return _containerName;
            }
            set
            {
                _containerName = value;
                RaisePropertyChanged("containerName");
            }
        }

        private string _resourceName;
        [JsonProperty(PropertyName = "resourceName")]
        public string resourceName
        {
            get
            {
                return _resourceName;
            }
            set
            {
                _resourceName = value;
                RaisePropertyChanged("resourceName");
            }
        }

        private string _sasQueryString;
        [JsonProperty(PropertyName = "sasQueryString")]
        public string sasQueryString
        {
            get
            {
                return _sasQueryString;
            }
            set
            {
                _sasQueryString = value;
                RaisePropertyChanged("sasQueryString");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
