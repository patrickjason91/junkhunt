﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace THApp.Resources
{
    public class AppIconToIMageConverter : IValueConverter
    {
            //string var = (string)value;

            //if (var == null)
            //{
            //    return "../Assets/UnknownAppIcon.png";
            //}
            //return value;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // value is the data from the source object.
            string var = (string)value;

            if (var == null)
            {
                return "Assets/UnknownAppIcon.png";
            }
            return value;

        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
