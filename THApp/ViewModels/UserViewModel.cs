﻿using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Q42.WinRT.Portable.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Device.Location;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THApp.Models;

namespace THApp.ViewModels
{
    public class UserViewModel : INotifyPropertyChanged
    {
        private MobileServiceCollection<Users, Users> _users;
        public MobileServiceCollection<Users, Users> users
        {
            get
            {
                return _users;
            }
            set
            {
                _users = value;
                RaisePropertyChanged("users");
            }
        }
        public IMobileServiceTable<Users> UsersTable = App.MobileService.GetTable<Users>();

        private Users _selectedUser;
        public Users selectedUser
        {
            get
            {
                return _selectedUser;
            }
            set
            {
                _selectedUser = value;
                RaisePropertyChanged("selectedUser");
            }
        }

        private GeoCoordinate _myCoordinate;
        public GeoCoordinate MyCoordinate
        {
            get
            {
                return _myCoordinate;
            }
            set
            {
                _myCoordinate = value;
                RaisePropertyChanged("myCoordinate");
            }
        }

        DataLoader _dataL = new DataLoader();

        public DataLoader dataLoader
        {
            get { return _dataL; }
            set { _dataL = value; }
        }

        #region Login

        internal async Task<Users> FindUser(int id)
        {
            users = await UsersTable.Where(UserItem => UserItem.Id == id).ToCollectionAsync();

            return users[0];
        }
        internal async Task<bool> CheckUser(string emailAddress, string password)
        {
            users = await UsersTable.Where(UserItem => UserItem.EmailAddress == emailAddress && UserItem.Password == password).ToCollectionAsync().ConfigureAwait(false);

            selectedUser = new Users();
            if (users.Count > 0)
            {
                selectedUser = users[0];
                return true;
            }

            selectedUser = null;
            return false;
        }

        #endregion

        #region registration
        internal async Task<bool> CheckEmail(string emailAddress)
        {
            users = await UsersTable.Where(UserItem => UserItem.EmailAddress == emailAddress).ToCollectionAsync().ConfigureAwait(false);

            if (users.Count > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

     
        internal void SaveCredentials(string emailAddress, string password)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            
            settings["EmailAddress"] = emailAddress;
            settings["Password"] = password;
            settings.Save();
        }

        internal void RemoveCredentials()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            settings.Clear();
            settings.Save();

        }

        internal List<string> LoadCredentials()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            List<string> credentials = new List<string>();

            if (settings.Contains("EmailAddress") && settings.Contains("Password"))
            {
                credentials.Add(settings["EmailAddress"].ToString());
                credentials.Add(settings["Password"].ToString());
            }
            else
            {
                credentials = null;
            }

            return credentials;
        }

        internal async Task UpdateInformation(Users user, Stream imageStream)
        {
            if (imageStream != null)
            {
                imageStream.Position = 0;
                user.containerName = "userimage" + RandomString(4);
                await UsersTable.UpdateAsync(user);
                // If we have a returned SAS, then upload the blob.
                if (!string.IsNullOrEmpty(user.sasQueryString))
                {
                    // Get the URI generated that contains the SAS 
                    // and extract the storage credentials.
                    StorageCredentials cred = new StorageCredentials(user.sasQueryString);
                    var imageUri = new Uri(user.imageUri);

                    // Instantiate a Blob store container based on the info in the returned item.
                    CloudBlobContainer container = new CloudBlobContainer(
                        new Uri(string.Format("https://{0}/{1}",
                            imageUri.Host, user.containerName)), cred);

                    CloudBlockBlob blobFromSASCredential =
                        container.GetBlockBlobReference(user.resourceName);
                    await blobFromSASCredential.UploadFromStreamAsync(imageStream);

                    imageStream = null;
                }
            }
            else
            {
                await UsersTable.UpdateAsync(user);
            }
        }
        private static Random random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

   
}
