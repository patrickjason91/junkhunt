﻿using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Q42.WinRT.Portable.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THApp.Models;

namespace THApp.ViewModels
{
    public class JunkViewModel : INotifyPropertyChanged
    {
        Stream imageStream = null;

        private MobileServiceCollection<Junks, Junks> _junks;
        public MobileServiceCollection<Junks, Junks> junks
        {
            get
            {
                return _junks;
            }
            set
            {
                _junks = value;
                RaisePropertyChanged("junks");
            }
        }
        public IMobileServiceTable<Junks> JunksTable = App.MobileService.GetTable<Junks>();
        private Junks _selectedJunk;
        public Junks selectedJunk
        {
            get
            {
                return _selectedJunk;
            }
            set
            {
                _selectedJunk = value;
                RaisePropertyChanged("selectedJunk");
            }
        }
       
        DataLoader _dataL = new DataLoader();

        public DataLoader dataLoader
        {
            get { return _dataL; }
            set { _dataL = value; }
        }

        internal async Task AddJunks(Junks junk)
        {
            imageStream.Position = 0;
            imageStream = App.GetResourceStream(new Uri("Assets/PlaceImage.png", UriKind.Relative)).Stream;
            
            junk.containerName = "junkimage" + RandomString(4);
            junk.resourceName = "picture";

            await JunksTable.InsertAsync(junk).ConfigureAwait(false);
            // If we have a returned SAS, then upload the blob.
            if (!string.IsNullOrEmpty(junk.sasQueryString))
            {
                // Get the URI generated that contains the SAS 
                // and extract the storage credentials.
                StorageCredentials cred = new StorageCredentials(junk.sasQueryString);
                var imageUri = new Uri(junk.imageUri);

                // Instantiate a Blob store container based on the info in the returned item.
                CloudBlobContainer container = new CloudBlobContainer(
                    new Uri(string.Format("https://{0}/{1}",
                        imageUri.Host, junk.containerName)), cred);

                CloudBlockBlob blobFromSASCredential =
                    container.GetBlockBlobReference(junk.resourceName);
                await blobFromSASCredential.UploadFromStreamAsync(imageStream);

                imageStream = null;
            }

            //await JunksTable.InsertAsync(junk).ConfigureAwait(false);
        }

        internal async Task UpdateJunk(Junks junk, Stream imageStream)
        {

            if (imageStream != null)
            {
                imageStream.Position = 0;
                junk.containerName = "junkimage" + RandomString(4);
                await JunksTable.UpdateAsync(junk);
                // If we have a returned SAS, then upload the blob.
                if (!string.IsNullOrEmpty(junk.sasQueryString))
                {
                    // Get the URI generated that contains the SAS 
                    // and extract the storage credentials.
                    StorageCredentials cred = new StorageCredentials(junk.sasQueryString);
                    var imageUri = new Uri(junk.imageUri);

                    // Instantiate a Blob store container based on the info in the returned item.
                    CloudBlobContainer container = new CloudBlobContainer(
                        new Uri(string.Format("https://{0}/{1}",
                            imageUri.Host, junk.containerName)), cred);

                    CloudBlockBlob blobFromSASCredential =
                        container.GetBlockBlobReference(junk.resourceName);
                    await blobFromSASCredential.UploadFromStreamAsync(imageStream);

                    imageStream = null;
                }
            }
            else
            {
                await JunksTable.UpdateAsync(junk);
            }
        }

        internal async Task LoadAllJunks()
        {
            junks = await JunksTable.ToCollectionAsync().ConfigureAwait(false);
        }
        internal async Task LoadMyJunks()
        {
            junks = await JunksTable.Where(junk => junk.userID == ((App)(App.Current))._userViewModel.selectedUser.Id).ToCollectionAsync().ConfigureAwait(false);
        }

        private static Random random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
     }
}
