﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace THApp.Views
{
    public partial class LoginPage : PhoneApplicationPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            List<string> credentials = new List<string>();

            credentials = ((App)(App.Current))._userViewModel.LoadCredentials();

            if (credentials != null)
            {
                var x = ((App)(App.Current))._userViewModel.dataLoader.LoadAsync(
                                    async () =>
                                    {
                                        if (await ((App)(App.Current))._userViewModel.CheckUser(credentials[0], credentials[1]))
                                        {
                                            this.NavigationService.Navigate(new Uri("/Views/MainPage.xaml", UriKind.Relative));
                                        }
                                        else
                                        {
                                            MessageBox.Show("Invalid email or password");
                                        }
                                        return "success";
                                    }, (result) =>
                                    {

                                    }, (exc) =>
                                    {
                                        MessageBox.Show("A problem has been encountered during the login process." + " Make sure your internet connection is stable.");
                                    });
            }

            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }


        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtBoxEmailAddress.Text) || String.IsNullOrEmpty(txtBoxPassword.Password))
            {
                MessageBox.Show("You must fill in all of the fields.");
            }
            else
            {
                var x = ((App)(App.Current))._userViewModel.dataLoader.LoadAsync(
                    async () =>
                    {
                        if (await ((App)(App.Current))._userViewModel.CheckUser(txtBoxEmailAddress.Text, txtBoxPassword.Password))
                        {
                            ((App)(App.Current))._userViewModel.SaveCredentials(txtBoxEmailAddress.Text, txtBoxPassword.Password);
                            this.NavigationService.Navigate(new Uri("/Views/MainPage.xaml", UriKind.Relative));
                        }
                        else
                        {
                            MessageBox.Show("Invalid email or password");
                        }
                        return "success";
                    }, (result) =>
                    {

                    }, (exc) =>
                    {
                        MessageBox.Show("A problem has been encountered during the login process." + " Make sure your internet connection is stable.");
                    });

            }

        }

        private void hlbtnNewUser_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Views/RegisterPage.xaml", UriKind.Relative));
        }   
    }
}