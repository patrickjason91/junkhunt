﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace THApp.Views
{
    public partial class MyProfile : PhoneApplicationPage
    {
        public MyProfile()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.DataContext = ((App)(App.Current))._userViewModel.selectedUser;
        }

        private void logoutClick(object sender, EventArgs e)
        {
            ((App)(App.Current))._userViewModel.RemoveCredentials();
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
            this.NavigationService.Navigate(new Uri("/Views/LoginPage.xaml", UriKind.Relative));
        }

        private void editProfileClick(object sender, EventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Views/EditProfile.xaml", UriKind.Relative));
        }
    }
}