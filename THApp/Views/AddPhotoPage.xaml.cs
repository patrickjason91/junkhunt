﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Devices;
using Microsoft.Phone.Tasks;
using System.IO;
using Nokia.Graphics.Imaging;
using System.Windows.Media.Imaging;
using THApp.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.Phone.Media.Capture;
using System.Diagnostics;

namespace THApp.Views
{
    public partial class AddPhotoPage : PhoneApplicationPage
    {
        ObservableCollection<FilterPreview> filters;
        FilterPreview selectedFilterPreview = null;
        IFilter selectedFilter = null;
        public AddPhotoPage()
        {
            InitializeComponent();

            ReadyFilterChoicesList();
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            ToggleAppbar(false);
        }

        private void FromCameraClick(object sender, RoutedEventArgs e)
        {
            FirstPanelGrid.Visibility = Visibility.Collapsed;
            CameraPanelGrid.Visibility = Visibility.Visible;
            SetupCameraView();
        }

        void SetupCameraView()
        {
            PhotoCamera camera = new PhotoCamera();
            PhotoViewfinderBrush.SetSource(camera);
        }
        
        async void ReadyFilterChoicesList()
        {
            var stream = Application.GetResourceStream(new Uri("Assets/earth.png", UriKind.RelativeOrAbsolute)).Stream;

            List<FilterPreview> filters = new List<FilterPreview>();
            filters.Add(new FilterPreview { Filter = new AntiqueFilter(), FilterName = "Antique", PhotoStream = stream });
            filters.Add(new FilterPreview { Filter = new VignettingFilter(), FilterName = "Vignette", PhotoStream = stream });
            filters.Add(new FilterPreview { Filter = new MonoColorFilter(), FilterName = "Mono" });
            filters.Add(new FilterPreview { Filter = new GrayscaleFilter(), FilterName = "Grayscale" });
            filters.Add(new FilterPreview { Filter = new AutoEnhanceFilter(), FilterName = "Auto-enhance" });
            filters.Add(new FilterPreview { Filter = new ColorBoostFilter(), FilterName = "Color Boost" });
            filters.Add(new FilterPreview { Filter = new MagicPenFilter(), FilterName = "Magic Pen" });
            filters.Add(new FilterPreview { Filter = new LomoFilter(0.5, 0.5, LomoVignetting.High, LomoStyle.Yellow), FilterName = "Lomo" });
            filters.Add(new FilterPreview { Filter = new StampFilter(4, 0.3), FilterName = "Stamp" });
            filters.Add(new FilterPreview { Filter = new SepiaFilter(), FilterName = "Sepia" });

            await RenderThumbnailsAsync(stream, 136, filters, FiltersListPanel);
        }

        private async Task RenderThumbnailsAsync(Stream bitmap, int side, List<FilterPreview> list, WrapPanel panel)
        {
            using (StreamImageSource source = new StreamImageSource(bitmap))
            using (FilterEffect effect = new FilterEffect(source))
            {
                foreach (FilterPreview filter in list)
                {
                    effect.Filters = filter.Filters;

                    WriteableBitmap writeableBitmap = new WriteableBitmap(side, side);

                    using (WriteableBitmapRenderer renderer = new WriteableBitmapRenderer(effect, writeableBitmap))
                    {
                        await renderer.RenderAsync();

                        writeableBitmap.Invalidate();

                        PhotoThumbnail photoThumbnail = new PhotoThumbnail()
                        {
                            Bitmap = writeableBitmap,
                            Text = filter.FilterName,
                            Width = side,
                            Height = side,
                            Margin = new Thickness(6)
                        };

                        photoThumbnail.Tap += (object sender, System.Windows.Input.GestureEventArgs e) =>
                        {
                            selectedFilterPreview = filter;
                            CustomMessageBox mBox = new CustomMessageBox 
                            {
                                Message = "Where to get the photo?"
                            };
                            mBox.IsLeftButtonEnabled = mBox.IsRightButtonEnabled = true;
                            //mBox.RightButtonContent = "From Camera";
                            mBox.LeftButtonContent = "From Gallery";
                            mBox.Dismissed += (s, ev) =>
                            {
                                switch (ev.Result)
                                {
                                    case CustomMessageBoxResult.LeftButton:
                                        PhotoChooserTask task = new PhotoChooserTask();
                                        task.Completed += taskCompleted;
                                        task.Show();
                                        break;
                                    case CustomMessageBoxResult.None:
                                        break;
                                    case CustomMessageBoxResult.RightButton:
                                        GoToCameraDisplay();
                                        break;
                                    default:
                                        break;
                                }
                            };
                            mBox.Show();
                        };
                        FiltersListPanel.Children.Add(photoThumbnail);
                    }
                }
            }
        }

        private void taskCompleted(object sender, PhotoResult e)
        {
            if (e.Error == null && e.ChosenPhoto != null && e.TaskResult == TaskResult.OK)
            {
                ProcessImageByEffect(e.ChosenPhoto);
            }
        }

        void GoToCameraDisplay()
        {
            this.FirstPanelGrid.Visibility = Visibility.Collapsed;
            this.CameraPanelGrid.Visibility = Visibility.Visible;
            InitializeCamera();
        }

        PhotoCaptureDevice _photoCaptureDevice;
        CameraStreamSource source;
        MediaElement element;
        CameraCaptureSequence camSeq;
        MemoryStream captureStream;

        async void InitializeCamera()
        {
            captureStream = new MemoryStream();
            var res = PhotoCaptureDevice.GetAvailablePreviewResolutions(CameraSensorLocation.Back).Last();
            Debug.WriteLine(res.ToString());
            _photoCaptureDevice = await PhotoCaptureDevice.OpenAsync(CameraSensorLocation.Back, res);
            
            camSeq = _photoCaptureDevice.CreateCaptureSequence(1);
            
            camSeq.Frames[0].CaptureStream = captureStream.AsOutputStream();

            await _photoCaptureDevice.PrepareCaptureSequenceAsync(camSeq);
            await _photoCaptureDevice.SetPreviewResolutionAsync(res);
            source = new CameraStreamSource(selectedFilterPreview, res);
            source.CaptureDevice = _photoCaptureDevice;
            element = new MediaElement();
            element.SetSource(source);
            element.Stretch = System.Windows.Media.Stretch.UniformToFill;
            PhotoViewfinderBrush.SetSource(element);
            ToggleAppbar(true);
        }

        async void ProcessImageByEffect(Stream stream)
        {
            var filterSelected = selectedFilterPreview.Filters;
            using (var imgSrc = new StreamImageSource(stream))
            using (var effect = new FilterEffect(imgSrc))
            {
                effect.Filters = filterSelected;
                using (WriteableBitmapRenderer renderer = new WriteableBitmapRenderer(effect))
                {
                    renderer.OutputOption = OutputOption.PreserveAspectRatio;
                    renderer.WriteableBitmap = new WriteableBitmap(400, 300);
                    var wb = await renderer.RenderAsync();
                    SetPhotoStreamAndGoBack(wb);
                }
            }
        }

        private void SetPhotoStreamAndGoBack(WriteableBitmap wb)
        {
            MemoryStream ms = new MemoryStream();
            wb.SaveJpeg(ms, wb.PixelWidth, wb.PixelHeight, 0, 100);
            App.PhotoFiltered = ms;
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }

        private void Uninitialize()
        {
            if (element != null)
            {
                element.Source = null;
                element = null;
            }
            if (source != null)
            {
                source = null;
            }
            if (_photoCaptureDevice != null)
            {
                _photoCaptureDevice.Dispose();
                _photoCaptureDevice = null;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            Uninitialize();
        }

        private async void CaptureClick(object sender, EventArgs e)
        {
            await camSeq.StartCaptureAsync();

            // Set the stream position to the beginning.
            captureStream.Seek(0, SeekOrigin.Begin);
            ProcessImageByEffect(captureStream);
        }

        void ToggleAppbar(bool visible)
        {
            this.ApplicationBar.IsVisible = visible;
        }

        private void FromGalleryBtnClick(object sender, RoutedEventArgs e)
        {

        }
    }
}