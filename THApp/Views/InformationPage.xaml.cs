﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using THApp.Models;

namespace THApp.Views
{
    public partial class InformationPage : PhoneApplicationPage
    {
        public InformationPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Users user = new Users();

            var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                                                  async () =>
                                                  {
                                                      //await ((App)(App.Current))._junkViewModel.AddJunks(junk);
                                                      user = await ((App)(App.Current))._userViewModel.FindUser(((App)(App.Current))._junkViewModel.selectedJunk.userID);

                                                      return "success";
                                                  }, (result) =>
                                                  {
                                                      this.DataContext = ((App)(App.Current))._junkViewModel.selectedJunk;
                                                      seenBy.Text = user.FirstName + " " + user.LastName;
                                                  }, (exc) =>
                                                  {
                                                      //this.NavigationService.GoBack();
                                                  });

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            ((App)(App.Current))._junkViewModel.selectedJunk.othersApproved = true;

            var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                   async () =>
                   {
                       await ((App)(App.Current))._junkViewModel.UpdateJunk(((App)(App.Current))._junkViewModel.selectedJunk, null); //Change Stream
                       return "success";
                   }, (result) =>
                   {
                       this.NavigationService.GoBack();
                   }, (exc) =>
                   {
                       MessageBox.Show("A problem has been encountered during the approval process." + " Make sure your internet connection is stable.");
                   });

        }

    }
}