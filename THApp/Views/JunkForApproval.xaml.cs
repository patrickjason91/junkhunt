﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using THApp.Models;

namespace THApp.Views
{
    public partial class JunkForApproval : PhoneApplicationPage
    {
        public JunkForApproval()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.DataContext = ((App)(App.Current))._junkViewModel.selectedJunk;
        }

        private void denyClick(object sender, EventArgs e)
        {
            MessageBoxResult m = MessageBox.Show("This action cannot be undo.", "Junk Approval", MessageBoxButton.OKCancel);
            if (m == MessageBoxResult.OK)
            {
                Junks junk = ((App)(App.Current))._junkViewModel.selectedJunk;
                junk.ownersApproved = false;
                junk.othersApproved = false;

                var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                    async () =>
                    {
                        await ((App)(App.Current))._junkViewModel.UpdateJunk(junk, null);
                        return "success";
                    }, (result) =>
                    {
                        this.NavigationService.GoBack();
                    }, (exc) =>
                    {
                        MessageBox.Show("A problem has been encountered during the approval process." + " Make sure your internet connection is stable.");
                    });
            }
        }

        private void approveClick(object sender, EventArgs e)
        {
            MessageBoxResult m = MessageBox.Show("This action cannot be undo.", "Junk Approval", MessageBoxButton.OKCancel);
            if (m == MessageBoxResult.OK)
            {
                Junks junk = ((App)(App.Current))._junkViewModel.selectedJunk;
                junk.ownersApproved = true;
                junk.othersApproved = true;

                var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                    async () =>
                    {
                        await ((App)(App.Current))._junkViewModel.UpdateJunk(junk, null);
                        return "success";
                    }, (result) =>
                        {
                            this.NavigationService.GoBack();
                        }, (exc) =>
                            {
                                MessageBox.Show("A problem has been encountered during the approval process." + " Make sure your internet connection is stable.");
                            });
            }
        }
    }
}