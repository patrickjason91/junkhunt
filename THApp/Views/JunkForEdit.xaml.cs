﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using THApp.Models;
using System.IO;
using System.Windows.Media.Imaging;

namespace THApp.Views
{
    public partial class JunkForEdit : PhoneApplicationPage
    {
        public JunkForEdit()
        {
            InitializeComponent();
        }

        Stream imageStream = null;
        bool haveLocation = false;
        double latitutde;
        double longtitude;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if ((Application.Current as App).SelectedLocation != null)
            {
                txtBlkLocation.Text = "Lat: " + (Application.Current as App).SelectedLocation.Latitude.ToString() + Environment.NewLine + "Long: " + (Application.Current as App).SelectedLocation.Longitude.ToString();
                latitutde = (Application.Current as App).SelectedLocation.Latitude;
                longtitude = (Application.Current as App).SelectedLocation.Longitude;
                haveLocation = true;
                (Application.Current as App).SelectedLocation = null;
            }
            this.DataContext = ((App)(App.Current))._junkViewModel.selectedJunk;

            if (App.PhotoFiltered != null)
            {
                imageStream = App.PhotoFiltered;
                var bi = new BitmapImage();
                bi.SetSource(imageStream);
                PlaceholderImage.Source = bi;
            }
            else
            {
                imageStream = null;
            }
        }

        private void HolderPhotoClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Views/AddPhotoPage.xaml", UriKind.Relative));
        }

        private void AddJunkSaveClick(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtBoxName.Text) ||
                String.IsNullOrEmpty(txtBoxDescription.Text) ||
                String.IsNullOrEmpty(txtBoxDescription.Text))
            {

                MessageBox.Show("You must fill in all of the fields.");
            }
            else
            {
                Junks junk = ((App)(App.Current))._junkViewModel.selectedJunk;
                junk.name = txtBoxName.Text;
                junk.description = txtBoxDescription.Text;
                junk.location = txtBlkLocation.Text;
                if (haveLocation)
                {
                    junk.latitude = latitutde;
                    junk.longtitude = longtitude;
                }

                var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                    async () =>
                    {
                        await ((App)(App.Current))._junkViewModel.UpdateJunk(junk, imageStream); //Change Stream
                        return "success";
                    }, (result) =>
                    {
                        this.NavigationService.GoBack();
                    }, (exc) =>
                    {
                        MessageBox.Show("A problem has been encountered during the approval process." + " Make sure your internet connection is stable.");
                    });
            }


        }

        private void btnLocationSelector_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Views/LocationSelector.xaml", UriKind.Relative));
        }


    }
}