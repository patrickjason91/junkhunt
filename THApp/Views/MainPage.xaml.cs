﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using THApp.Resources;
using System.Device.Location;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using THApp.Models;
using Microsoft.Phone.Maps.Toolkit;
using System.Windows.Media.Imaging;

namespace THApp.Views
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ShowMyLocationOnTheMap();
            ShowPushpins();
            DataBinding();

            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
        }

        private void DataBinding()
        {
            var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                 async () =>
                 {
                     await ((App)(App.Current))._junkViewModel.LoadAllJunks();
                     return "success";

                 }, (result) =>
                 {
                     this.JunkNearbyList.DataContext = from Junks in ((App)(App.Current))._junkViewModel.junks where Junks.userID != ((App)(App.Current))._userViewModel.selectedUser.Id && Junks.othersApproved != true select Junks; 
                 }, (exc) =>
                 {

                 });


        }


        public GeoCoordinate MyCoordinate = null;

        private void ShowPushpins()
        {
            var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                async () =>
                {
                    await ((App)(App.Current))._junkViewModel.LoadAllJunks();
                    return "success";
                }, (result) =>
                    {

                        MapLayer layer = new MapLayer();

                        for (int i = 0; i < ((App)(App.Current))._junkViewModel.junks.Count; i++)
                        {
                            if (((App)(App.Current))._junkViewModel.junks[i].othersApproved == true)
                            {
                                GeoCoordinate dataMyCoordinate = new GeoCoordinate(((App)(App.Current))._junkViewModel.junks[i].latitude, ((App)(App.Current))._junkViewModel.junks[i].longtitude);
                                double value = ((App)(App.Current))._userViewModel.MyCoordinate.GetDistanceTo(dataMyCoordinate);


                                MapOverlay overlay = new MapOverlay();
                                Image pinOverlayImage = new Image();
                                pinOverlayImage.Source = new BitmapImage(new Uri("/Assets/logo.png", UriKind.Relative));

                                overlay.GeoCoordinate = new GeoCoordinate(((App)(App.Current))._junkViewModel.junks[i].latitude, ((App)(App.Current))._junkViewModel.junks[i].longtitude);
                                overlay.Content = pinOverlayImage;
                                overlay.PositionOrigin = new Point(0.0, 0.0);
                                pinOverlayImage.Opacity = 0.8;
                                pinOverlayImage.Width = 50;
                                pinOverlayImage.Height = 50;
                                layer.Add(overlay);
                                pinOverlayImage.Tag = ((App)(App.Current))._junkViewModel.junks[i].Id;
                                pinOverlayImage.Tap += pinOverlayImage_Tap;
                            }

                        }

                        MyMap.Layers.Add(layer);
                    }, (exc) =>
                        {

                        });
            
        }

        void pinOverlayImage_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            
        }

        private double _accuracy = 0.0;
        private async void ShowMyLocationOnTheMap()
        {
            Geolocator myGeolocator = new Geolocator();
            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
            GeoCoordinate myGeoCoordinate =
               CoordinateConverter.ConvertGeocoordinate(myGeocoordinate);

            this.MyMap.Center = myGeoCoordinate;
            this.MyMap.ZoomLevel = 20;

            ((App)(App.Current))._userViewModel.MyCoordinate = myGeoCoordinate;
            DrawAccuracyRadius(myGeoCoordinate);
        }

        private void DrawAccuracyRadius(GeoCoordinate myGeoCoordinate)
        {
            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;

            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;

            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);

            MyMap.Layers.Add(myLocationLayer);
            
        }


        private void AddJunkClick(object sender, EventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Views/AddJunkPage.xaml", UriKind.Relative));
            
        }

        private void MainMap_Loaded(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "5QmCa3Bj43xmPuPGANTS";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "oobols4fAzI7V9UK8iK9Wg";
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            PageOrientation orientation = e.Orientation;

            if ((orientation & PageOrientation.Landscape) == PageOrientation.Landscape)
            {
                MyMap.IsEnabled = true;
                GridOverlay.Visibility = Visibility.Collapsed;
            }
            else if ((orientation & PageOrientation.Portrait)  == PageOrientation.Portrait) 
            {
                MyMap.IsEnabled = false;
                GridOverlay.Visibility = Visibility.Visible;
                
            }
        }

        private void myProfile_Click(object sender, EventArgs e)
        {

            this.NavigationService.Navigate(new Uri("/Views/MyProfile.xaml", UriKind.Relative));
        }

        private void myJunks_Click(object sender, EventArgs e)
        {

            this.NavigationService.Navigate(new Uri("/Views/UserPage.xaml", UriKind.Relative));
        }

        private void JunkNearbyList_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Junks junk = (Junks)((ListBox)sender).SelectedItem;
            if (junk != null)
            {
                ((App)(App.Current))._junkViewModel.selectedJunk = junk;
                this.NavigationService.Navigate(new Uri("/Views/InformationPage.xaml", UriKind.Relative));
            }

        }
    }

    public static class CoordinateConverter
    {
        public static GeoCoordinate ConvertGeocoordinate(Geocoordinate geocoordinate)
        {
            return new GeoCoordinate
                (
                geocoordinate.Latitude,
                geocoordinate.Longitude,
                geocoordinate.Altitude ?? Double.NaN,
                geocoordinate.Accuracy,
                geocoordinate.AltitudeAccuracy ?? Double.NaN,
                geocoordinate.Speed ?? Double.NaN,
                geocoordinate.Heading ?? Double.NaN
                );
        }
    }
}