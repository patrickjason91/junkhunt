﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using THApp.Models;

namespace THApp.Views
{
    public partial class UserPage : PhoneApplicationPage
    {
        public UserPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                async () =>
                {
                    await ((App)(App.Current))._junkViewModel.LoadMyJunks();
                    return "success";

                }, (result) =>
                    {
                        lbForApproval.DataContext = from Junks in ((App)(App.Current))._junkViewModel.junks where Junks.othersApproved == true && Junks.ownersApproved == false select Junks;
                        lbUnpick.DataContext = from Junks in ((App)(App.Current))._junkViewModel.junks where Junks.othersApproved == false && Junks.ownersApproved == false select Junks;
                        lbAllList.DataContext = ((App)(App.Current))._junkViewModel.junks;
                    }, (exc) =>
                        {

                        });

            try
            {
                lbAllList.SelectedItem = null;
                lbForApproval.SelectedItem = null;
                lbUnpick.SelectedItem = null;
            }
            catch
            {

            }
            
        }

        private void lbUnpick_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Junks junk = (Junks)((ListBox)sender).SelectedItem;
            if (junk != null)
            {
                ((App)(App.Current))._junkViewModel.selectedJunk = junk;

                this.NavigationService.Navigate(new Uri("/Views/JunkForEdit.xaml", UriKind.Relative));
            }
        }

        private void lbAllList_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Junks junk = (Junks)((ListBox)sender).SelectedItem;
            if (junk != null)
            {
                if (junk.othersApproved == false && junk.ownersApproved == false)
                {
                    //Uncollected

                    ((App)(App.Current))._junkViewModel.selectedJunk = junk;

                    this.NavigationService.Navigate(new Uri("/Views/JunkForEdit.xaml", UriKind.Relative));
                }
                else if (junk.othersApproved == true && junk.ownersApproved == false)
                {
                    //For Approval

                    ((App)(App.Current))._junkViewModel.selectedJunk = junk;


                    this.NavigationService.Navigate(new Uri("/Views/JunkForApproval.xaml", UriKind.Relative));
                }
            }
        }

        private void lbForApproval_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Junks junk = (Junks)((ListBox)sender).SelectedItem;
            if (junk != null)
            {
                ((App)(App.Current))._junkViewModel.selectedJunk = junk;

                this.NavigationService.Navigate(new Uri("/Views/JunkForApproval.xaml", UriKind.Relative));
            }
        }
    }
}