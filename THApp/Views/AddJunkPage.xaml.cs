﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using THApp.Models;
using System.Threading.Tasks;
using System.IO;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Text;
using System.Windows.Media.Imaging;

namespace THApp.Views
{
    public partial class AddJunkPage : PhoneApplicationPage
    {
        public AddJunkPage()
        {
            InitializeComponent();
        }

        Stream imageStream = null;
        private MobileServiceCollection<Junks, Junks> users;
        private IMobileServiceTable<Junks> JunksTable = App.MobileService.GetTable<Junks>();

        bool haveLocation = false;
        double latitutde;
        double longtitude;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if ((Application.Current as App).SelectedLocation != null)
            {
                txtBlkLocation.Text = "Lat: " + (Application.Current as App).SelectedLocation.Latitude.ToString() + Environment.NewLine + "Long: " + (Application.Current as App).SelectedLocation.Longitude.ToString();
                latitutde = (Application.Current as App).SelectedLocation.Latitude;
                longtitude = (Application.Current as App).SelectedLocation.Longitude;
                haveLocation = true;
                (Application.Current as App).SelectedLocation = null;
            }

            if (App.PhotoFiltered != null)
            {
                imageStream = App.PhotoFiltered;
                var bi = new BitmapImage();
                bi.SetSource(imageStream);
                PlaceholderImage.Source = bi;
            }
            else
            {
                imageStream = null;
            }
        }

        private void AddJunkSaveClick(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtBoxName.Text) || String.IsNullOrEmpty(txtBoxDescription.Text))
            {
                MessageBox.Show("You must fill in all of the fields.");
            }
            else if (haveLocation == false)
            {
                MessageBox.Show("Please select the location");
            }
            else
            {
                Junks junk = new Junks()
                {
                    name = txtBoxName.Text,
                    description = txtBoxDescription.Text,
                    dateUploaded = DateTime.Now,
                    location = txtBlkLocation.Text,
                    userID = ((App)(App.Current))._userViewModel.selectedUser.Id,
                    latitude = latitutde,
                    longtitude = longtitude,
                    othersApproved = false,
                    ownersApproved = false
                };

                var x = ((App)(App.Current))._junkViewModel.dataLoader.LoadAsync(
                                         async () =>
                                         {
                                             //await ((App)(App.Current))._junkViewModel.AddJunks(junk);
                                             await InsertJunks(junk);
                                             return "success";
                                         }, (result) =>
                                         {
                                             this.NavigationService.GoBack();
                                         }, (exc) =>
                                         {
                                             MessageBox.Show("A problem has been encountered during the adding junk.");
                                             //this.NavigationService.GoBack();
                                         });

            }
        }

        private async Task InsertJunks(Junks junk)
        {
            imageStream.Position = 0;
            if (imageStream == null)
            {
                imageStream = App.GetResourceStream(new Uri("Assets/PlaceImage.png", UriKind.Relative)).Stream;
            }

            junk.containerName = "junkimage" + RandomString(4);
            junk.resourceName = "picture";

            await JunksTable.InsertAsync(junk);
            if (!string.IsNullOrEmpty(junk.sasQueryString))
            {
                StorageCredentials cred = new StorageCredentials(junk.sasQueryString);
                var imageUri = new Uri(junk.imageUri);

                CloudBlobContainer container = new CloudBlobContainer(
                    new Uri(string.Format("https://{0}/{1}",
                        imageUri.Host, junk.containerName)), cred);

                CloudBlockBlob blobFromSASCredential =
                    container.GetBlockBlobReference(junk.resourceName);
                await blobFromSASCredential.UploadFromStreamAsync(imageStream);

                imageStream = null;
            }

        }

        private void HolderPhotoClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Views/AddPhotoPage.xaml", UriKind.Relative));

        }

        private void btnLocationSelector_Click(object sender, RoutedEventArgs e)
        {

            this.NavigationService.Navigate(new Uri("/Views/LocationSelector.xaml", UriKind.Relative));
        }

        private static Random random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}