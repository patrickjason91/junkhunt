﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using THApp.Models;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace THApp.Views
{
    public partial class RegisterPage : PhoneApplicationPage
    {
        Stream imageStream = null;
        private MobileServiceCollection<Users, Users> users;
        private IMobileServiceTable<Users> UsersTable = App.MobileService.GetTable<Users>();


        public RegisterPage()
        {
            InitializeComponent();
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtBoxFirstName.Text) ||
                String.IsNullOrEmpty(txtBoxLastName.Text) ||
                String.IsNullOrEmpty(txtBoxEmailAddress.Text) ||
                String.IsNullOrEmpty(txtBoxPassword.Password))
            {
                MessageBox.Show("You must fill in all of the fields.");
            }
            else
            {
                if(((await ((App)(App.Current))._userViewModel.CheckEmail(txtBoxEmailAddress.Text) == false) && CheckEmail())) // User Doesn't Exist
                {
                    if (CheckPassword()) // Ok
                    {
                        Users user = new Users()
                        {
                            FirstName = txtBoxFirstName.Text,
                            LastName = txtBoxLastName.Text,
                            EmailAddress = txtBoxEmailAddress.Text,
                            Password = txtBoxPassword.Password
                        };

                        var x = ((App)(App.Current))._userViewModel.dataLoader.LoadAsync(
                            async () =>
                                {
                                    await InsertUser(user);
                                    return "success";
                                }, (result) =>
                                    {
                                        ((App)(App.Current))._userViewModel.selectedUser = new Users();
                                        ((App)(App.Current))._userViewModel.selectedUser = user;
                                        ((App)(App.Current))._userViewModel.SaveCredentials(txtBoxEmailAddress.Text, txtBoxPassword.Password);
                                        this.NavigationService.Navigate(new Uri("/Views/MainPage.xaml", UriKind.Relative));
                                    },(exc) =>
                                        {
                                            MessageBox.Show("A problem has been encountered during the registration." + " Make sure your internet connection is stable.");
                                        });

                    }
                    else
                    {
                        MessageBox.Show("Your password must be at least 6 characters long. Please try another.");
                    }
                }
                else
                {
                    if (CheckEmail() == false)
                    {
                        MessageBox.Show("Please enter a valid email address.");
                    }
                    else
                    {
                        MessageBox.Show("Sorry, it looks like " + txtBoxEmailAddress.Text + " belongs to an existing account. Please try another.");
                    }
                }
            }
        }

        private async Task InsertUser(Users user)
        {
            imageStream = App.GetResourceStream(new Uri("Assets/UnknownProfilePicture.png", UriKind.Relative)).Stream;
            imageStream.Position = 0;
            user.containerName = "userimage" + RandomString(4);
            user.resourceName = "picture";

            await UsersTable.InsertAsync(user);
            // If we have a returned SAS, then upload the blob.
            if (!string.IsNullOrEmpty(user.sasQueryString))
            {
                // Get the URI generated that contains the SAS 
                // and extract the storage credentials.
                StorageCredentials cred = new StorageCredentials(user.sasQueryString);
                var imageUri = new Uri(user.imageUri);

                // Instantiate a Blob store container based on the info in the returned item.
                CloudBlobContainer container = new CloudBlobContainer(
                    new Uri(string.Format("https://{0}/{1}",
                        imageUri.Host, user.containerName)), cred);
                
                CloudBlockBlob blobFromSASCredential =
                    container.GetBlockBlobReference(user.resourceName);
                await blobFromSASCredential.UploadFromStreamAsync(imageStream);

                imageStream = null;
            }

            //// Add the new item to the collection.
            //items.Add(todoItem);
            //TodoInput.Text = "";
        }

        private bool CheckPassword()
        {
            if (txtBoxPassword.Password.Length < 6)
            {
                return false;
            }
            return true;
        }

        private bool CheckEmail()
        {
            RegexUtilities util = new RegexUtilities();

            if (!util.IsValidEmail(txtBoxEmailAddress.Text) || String.Equals(txtBoxEmailAddress.Text, String.Empty))
            {
                //txtBlkErrorMessage.Text = "Please enter a valid email address.";
                return false;
            }
            

            return true;
        }

        private static Random random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

    }
}