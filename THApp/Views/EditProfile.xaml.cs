﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using THApp.Models;
using System.Windows.Media.Imaging;
using System.IO;

namespace THApp.Views
{
    public partial class EditProfile : PhoneApplicationPage
    {
        public EditProfile()
        {
            InitializeComponent();
        }

        Stream imageStream = null;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.DataContext = ((App)(App.Current))._userViewModel.selectedUser;
            if (App.PhotoFiltered != null)
            {
                imageStream = App.PhotoFiltered;
                var bi = new BitmapImage();
                bi.SetSource(imageStream);
                PlaceholderImage.Source = bi;
            }
            else
            {
                imageStream = null;
            }
        }

        private bool CheckPassword()
        {
            if (txtBoxPassword.Password.Length < 6)
            {
                return false;
            }
            return true;
        }

        private bool CheckEmail()
        {
            RegexUtilities util = new RegexUtilities();

            if (!util.IsValidEmail(txtBoxEmailAddress.Text) || String.Equals(txtBoxEmailAddress.Text, String.Empty))
            {
                //txtBlkErrorMessage.Text = "Please enter a valid email address.";
                return false;
            }
            return true;
        }

        private async void btnUpdateProfileClick(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtBoxFirstName.Text) ||
                   String.IsNullOrEmpty(txtBoxLastName.Text) ||
                   String.IsNullOrEmpty(txtBoxEmailAddress.Text))
            {
                MessageBox.Show("You must fill in all of the fields.");
            }
            else
            {
                if (((await((App)(App.Current))._userViewModel.CheckEmail(txtBoxEmailAddress.Text) == false) && CheckEmail()) || txtBoxEmailAddress.Text == ((App)(App.Current))._userViewModel.selectedUser.EmailAddress) // User Doesn't Exist
                {
                    if (CheckPassword() || String.IsNullOrEmpty(txtBoxPassword.Password)) // Ok
                    {
                        Users user = ((App)(App.Current))._userViewModel.selectedUser;
                        user.FirstName = txtBoxFirstName.Text;
                        user.LastName = txtBoxLastName.Text;
                        user.EmailAddress = txtBoxEmailAddress.Text;
                        if (txtBoxPassword.Password != String.Empty)
                        {
                            user.Password = txtBoxPassword.Password;
                        }
                        //{
                        //    FirstName = txtBoxFirstName.Text,
                        //    LastName = txtBoxLastName.Text,
                        //    EmailAddress = txtBoxEmailAddress.Text,
                        //    Password = txtBoxPassword.Password
                        //};

                        
                        var x = ((App)(App.Current))._userViewModel.dataLoader.LoadAsync(
                            async () =>
                            {
                                await ((App)(App.Current))._userViewModel.UpdateInformation(user, imageStream);
                                return "success";
                            }, (result) =>
                            {
                                ((App)(App.Current))._userViewModel.selectedUser = new Users();
                                ((App)(App.Current))._userViewModel.selectedUser = user;
                                if (txtBoxPassword.Password != String.Empty)
                                {
                                    ((App)(App.Current))._userViewModel.SaveCredentials(txtBoxEmailAddress.Text, txtBoxPassword.Password);
                                }
                                this.NavigationService.GoBack();
                            }, (exc) =>
                            {
                                MessageBox.Show("A problem has been encountered during the updating of your profile.");
                            });

                    }
                    else
                    {
                        MessageBox.Show("Your password must be at least 6 characters long. Please try another.");
                    }
                }
                else
                {
                    if (CheckEmail() == false)
                    {
                        MessageBox.Show("Please enter a valid email address.");
                    }
                    else
                    {
                        MessageBox.Show("Sorry, it looks like " + txtBoxEmailAddress.Text + " belongs to an existing account. Please try another.");
                    }
                }
            }
        }

        private void HolderPhotoClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Views/AddPhotoPage.xaml", UriKind.Relative));
        }
    }
}